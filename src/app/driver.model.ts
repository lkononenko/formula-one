export class Driver {
  id: string;
  firstName: string;
  lastName: string;
  url: string;
  dateOfBirth: string;
  nationality: string;

  constructor(driver) {
    this.id = driver.driverId;
    this.firstName = driver.givenName || '';
    this.lastName = driver.familyName || '';
    this.url = driver.url || '';
    this.dateOfBirth = driver.dateOfBirth || '';
    this.nationality = driver.nationality || '';
  }

  getFullName() {
    return this.firstName + ' ' + this.lastName;
  }
}
