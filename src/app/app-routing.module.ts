import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SeasonWinnersComponent } from './winners/season-winners/season-winners.component';
import { RaceWinnersComponent } from './winners/race-winners/race-winners.component';
import { PageNotFoundComponent } from './page-not-found.component';

const routes: Routes = [
  { path: '',  component: SeasonWinnersComponent },
  { path: 'season/:season', component: RaceWinnersComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
