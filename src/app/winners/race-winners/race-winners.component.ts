import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/switchMap';

import { WinnersService } from '../winners.service';
import { RaceWinner } from './race-winner.model';

@Component({
  selector: 'app-race-winners',
  templateUrl: './race-winners.component.html',
  styleUrls: ['../winners.component.scss']
})
export class RaceWinnersComponent implements OnInit, OnDestroy {
  errorMessage: string;
  winners: RaceWinner[];
  season: number;
  private sub: any;

  constructor(private winnersService: WinnersService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe((params: Params) => {
      this.season = +params['season'];

      this.getRaceWinners();
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getRaceWinners() {
    this.winnersService.getRaceWinners(this.season)
      .subscribe(
        winners => this.winners = winners,
        error => this.errorMessage = <any>error);
  }

  goBack() {
    this.router.navigate(['']);
  }
}
