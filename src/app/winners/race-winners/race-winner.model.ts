import { Driver } from "../../driver.model";

export class RaceWinner {
  round: number;
  season: number;
  driver: Driver;
  raceName: string;
  url: string;
  date: string;
  points: string;

  constructor(winnerData) {
    this.round = winnerData.round;
    this.season = winnerData.season;
    this.driver = new Driver(winnerData.Results[0].Driver);
    this.raceName = winnerData.raceName || '';
    this.url = winnerData.url || '';
    this.date = winnerData.date || '';
    this.points = winnerData.points || '';
  }
}
