import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { WinnersService } from '../winners.service';
import { SeasonWinner } from './season-winner.model';

@Component({
  selector: 'app-season-winners',
  templateUrl: './season-winners.component.html',
  styleUrls: ['../winners.component.scss']
})
export class SeasonWinnersComponent implements OnInit {
  errorMessage: string;
  winners: SeasonWinner[];

  constructor(
    private winnersService: WinnersService,
    private router: Router) { }

  ngOnInit() {
    this.getSeasonWinners();
  }

  getSeasonWinners() {
    this.winnersService.getSeasonWinners()
      .subscribe(
        winners => this.winners = winners,
        error => this.errorMessage = <any>error);
  }

  onClickSeason(season: number) {
    this.router.navigate(['/season', season]);
  }
}
