import { Driver } from "../../driver.model";

export class SeasonWinner {
  season: number;
  driver: Driver;
  points: string;
  wins: string;

  constructor(winnerData) {
    this.season = winnerData.season;
    this.driver = new Driver(winnerData.Driver);
    this.points = winnerData.points || '';
    this.wins = winnerData.wins || '';
  }
}
