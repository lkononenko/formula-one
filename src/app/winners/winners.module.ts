import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SeasonWinnersComponent } from './season-winners/season-winners.component';
import { RaceWinnersComponent } from './race-winners/race-winners.component';
import { WinnersService } from './winners.service';

@NgModule({
  imports: [CommonModule],
  providers: [WinnersService],
  declarations: [SeasonWinnersComponent, RaceWinnersComponent],
  exports:      [SeasonWinnersComponent]
})
export class WinnersModule { }
