import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { SeasonWinner } from './season-winners/season-winner.model';
import { CONST } from '../const';
import { RaceWinner } from './race-winners/race-winner.model';

@Injectable()
export class WinnersService {

  constructor(private http: Http) { }

  getSeasonWinners(): Observable<SeasonWinner[]> {
    let seasonWinnersUrl = CONST.API + 'f1/driverstandings/1.json?limit=100&offset=0';
    return this.http.get(seasonWinnersUrl)
      .map(this.extractSeasonWinners)
      .catch(this.handleError);
  }

  getRaceWinners(season: number): Observable<RaceWinner[]> {
    let raceWinnersUrl = CONST.API + 'f1/' + season + '/results/1.json';
    return this.http.get(raceWinnersUrl)
      .map(this.extractRaceWinners)
      .catch(this.handleError);
  }

  private extractSeasonWinners(res: Response) {
    let seasonWinners = [];
    let standings = res.json().MRData.StandingsTable.StandingsLists;
    standings.map(standing => {
      if (standing.season >= 2005 && standing.season <= 2015) {
        standing.DriverStandings[0].season = standing.season;
        seasonWinners.push(new SeasonWinner(standing.DriverStandings[0]));
      }
    });
    return seasonWinners;
  }

  private extractRaceWinners(res: Response) {
    let raceWinners = [];
    let races = res.json().MRData.RaceTable.Races;
    races.map(race => {
        raceWinners.push(new RaceWinner(race));
    });
    return raceWinners;
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      errMsg = `${error.status} - ${error.statusText || ''}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
