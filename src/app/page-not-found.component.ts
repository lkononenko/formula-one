import { Component } from '@angular/core';

@Component({
  template: '<div class="container"><h3>Page not found</h3></div>'
})
export class PageNotFoundComponent {}
